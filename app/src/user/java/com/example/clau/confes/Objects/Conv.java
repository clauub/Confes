package com.example.clau.confes.Objects;

import java.util.List;
import java.util.Map;

/**
 * Created by Clau on 2/5/2018.
 */

public class Conv {

    private String uid;
    private String titlu_intrebare;
    private boolean seen;
    private List<Chat> messages;

    public Conv(){}

    public Conv(String uid, String titlu_intrebare, boolean seen) {
        this.uid = uid;
        this.titlu_intrebare = titlu_intrebare;
        this.messages = messages;
        this.seen = seen;
    }
    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTitlu_intrebare() {
        return titlu_intrebare;
    }

    public void setTitlu_intrebare(String titlu_intrebare) {
        this.titlu_intrebare = titlu_intrebare;
    }

    public List<Chat> getMessages() {
        return messages;
    }

    public void setMessages(List<Chat> messages) {
        this.messages = messages;
    }
}
