package com.example.clau.confes.Objects;

import com.google.firebase.database.Exclude;

/**
 * Created by Clau on 1/19/2018.
 */

public class User {

    @Exclude
    public String phone;
    public String userID;
    public Boolean isBan;
    public Boolean isUser;
    static String chatWith = "";

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String phone, String userID, Boolean isBan, Boolean isUser) {
        this.phone = phone;
        this.userID = userID;
        this.isBan = isBan;
        this.isUser = isUser;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public Boolean getIsBan() {
        return isBan;
    }

    public void setIsBan(Boolean isBan) {
        isBan = isBan;
    }

    public Boolean getIsUser() {
        return isUser;
    }

    public void setIsUser(Boolean isUser) {
        isUser = isUser;
    }
}
