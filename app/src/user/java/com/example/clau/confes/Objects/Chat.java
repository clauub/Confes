package com.example.clau.confes.Objects;

/**
 * Created by Clau on 2/7/2018.
 */

public class Chat {

    public String text;
    public String phone;
    public String userID;
    public String title;
    public long time;

    public Chat() {
        // Default constructor required for calls to DataSnapshot.getValue(Chat.class)
    }

    public Chat(String text, String phone, String userID, String title, long time) {
        this.text = text;
        this.phone = phone;
        this.userID = userID;
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
