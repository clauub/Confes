package com.example.clau.confes.Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.clau.confes.Objects.Chat;
import com.example.clau.confes.R;
import com.google.firebase.auth.FirebaseAuth;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Clau on 2/2/2018.
 */

public class MessageAdapter extends ArrayAdapter<Chat> {

    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("HH:mm");
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();

    public MessageAdapter(Context context, int resource, List<Chat> objects) {
        super(context, resource, objects);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.item_message, parent, false);
        }

        String current_user_id = mAuth.getCurrentUser().getUid();

        LinearLayout messageLine = (LinearLayout) convertView.findViewById(R.id.messageLine);
        TextView messageTextView = (TextView) convertView.findViewById(R.id.message_text);
        TextView timeTextView = (TextView) convertView.findViewById(R.id.time_text);
        Chat message = getItem(position);


        if (current_user_id.equals(message.getUserID())) {

            messageLine.setGravity(Gravity.END);
            messageTextView.setVisibility(View.VISIBLE);
            timeTextView.setVisibility(View.VISIBLE);
            messageTextView.setText(message.getText());
            timeTextView.setText(SIMPLE_DATE_FORMAT.format(message.getTime()));
            messageTextView.setBackgroundResource(R.drawable.rounde_rectangle_from);
            timeTextView.setBackgroundResource(R.drawable.rounde_rectangle_from);

        }else{

            messageLine.setGravity(Gravity.START);
            messageTextView.setVisibility(View.VISIBLE);
            timeTextView.setVisibility(View.VISIBLE);
            messageTextView.setText(message.getText());
            timeTextView.setText(SIMPLE_DATE_FORMAT.format(message.getTime()));
            messageTextView.setBackgroundResource(R.drawable.rounded_rectangle);
            timeTextView.setBackgroundResource(R.drawable.rounded_rectangle);
        }
        return convertView;
    }
}

