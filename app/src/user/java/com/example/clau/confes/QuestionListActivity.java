package com.example.clau.confes;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.clau.confes.Adapters.ConvList;
import com.example.clau.confes.Objects.Chat;
import com.example.clau.confes.Objects.Conv;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.firebase.client.Firebase;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Utils.Constants;

public class QuestionListActivity extends AppCompatActivity {

    //Screen Views
    private ListView  mChatListView;
    private static final String TAG = "MainActivity";

    //Variables
    private String mCurrent_user_id;
    private String mCurrent_phone;
    private String mTitle;
    private String pushKey;

    private List<Conv> convList;
    private ArrayList<String> list_of_rooms = new ArrayList<>();
    private Conv mChat;
    private ValueEventListener mValueEventListener;

    //Firebase
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mChatDatabaseReference;
    private DatabaseReference mCurrentUserDatabaseReference;
    private DatabaseReference mUserDatabaseReference;
    private DatabaseReference mConvDatabase;
    private FirebaseListAdapter mChatAdapter;
    private FirebaseRecyclerAdapter mAdapter;
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Lista Intrebari");

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mChat = new Conv("","", false);

        //Initialize screen variables
        mChatListView = (ListView) findViewById(R.id.chatListView);
        convList = new ArrayList<>();
        mChatDatabaseReference = mFirebaseDatabase.getReference().child(Constants.CHAT_LOCATION);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createQuestion();
            }
        });

        //Add on click listener to line items
        mChatListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //getting the selected artist
                Conv conv = convList.get(position);

                Intent intent = new Intent(QuestionListActivity.this, ChatActivity.class);
                intent.putExtra(Constants.MESSAGE_ID, conv.getUid());
                intent.putExtra(Constants.CHAT_NAME, conv.getTitlu_intrebare());
                startActivity(intent);
                }
        });
    }

//    private void onSignedInInitialize(FirebaseUser user) {
//
//        mChatDatabaseReference = mFirebaseDatabase.getReference()
//                .child(Constants.USERS_LOCATION
//                        + "/" + mCurrent_user_id + "/"
//                        + Constants.CHAT_LOCATION );
//        mUserDatabaseReference = mFirebaseDatabase.getReference()
//                .child(Constants.USERS_LOCATION);
//
//        //Initialize screen variables
//        mChatListView = (ListView) findViewById(R.id.chatListView);
//
//        mChatAdapter = new FirebaseListAdapter<Conv>(this, Conv.class, R.layout.conv_layout, mChatDatabaseReference) {
//            @Override
//            protected void populateView(final View view, Conv conv, final int position) {
//                //Log.e("TAG", "");
//                //final Friend addFriend = new Friend(chat);
//                ((TextView) view.findViewById(R.id.messageTextView)).setText(conv.getTitlu_intrebare());
//
//                //Fetch last message from chat
//                final DatabaseReference messageRef =
//                        mFirebaseDatabase.getReference(Constants.MESSAGE_LOCATION
//                                + "/" + conv.getUid());
//
//                final TextView latestMessage = (TextView)view.findViewById(R.id.nameTextView);
//
//                messageRef.addChildEventListener(new ChildEventListener() {
//                    @Override
//                    public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
//                        Chat newMsg = dataSnapshot.getValue(Chat.class);
//                        latestMessage.setText((newMsg.getTime()) + ": " + newMsg.getText());
//                    }
//
//                    @Override
//                    public void onChildChanged(DataSnapshot dataSnapshot, String prevChildKey) {}
//
//                    @Override
//                    public void onChildRemoved(DataSnapshot dataSnapshot) {}
//
//                    @Override
//                    public void onChildMoved(DataSnapshot dataSnapshot, String prevChildKey) {}
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {}
//                });
//
//                //Replace this with the most recent message from the chat
//
//            }
//        };
//
//
//
//        mChatListView.setAdapter(mChatAdapter);
//
//        mValueEventListener = mChatDatabaseReference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                Conv chat = dataSnapshot.getValue(Conv.class);
//                //Check if any chats exists
//                if (chat == null) {
//                    //finish();
//                    return;
//                }
//                mChatAdapter.notifyDataSetChanged();
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//    }


    @Override
    protected void onStart() {
        super.onStart();

        mChatDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                convList.clear();
                for (DataSnapshot convSnapshot : dataSnapshot.getChildren()){
                    Conv conv = convSnapshot.getValue(Conv.class);
                    convList.add(conv);
                }
                ConvList adapter = new ConvList(QuestionListActivity.this, convList);
                mChatListView.setAdapter(adapter);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void onBackPressed(){
        // Your Code Here. Leave empty if you want nothing to happen on back press.
    }

    private void createQuestion() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Titlul Intrebarii:");

        final EditText input_field = new EditText(this);

        builder.setView(input_field);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


                mTitle = input_field.getText().toString();

                mCurrent_user_id = mAuth.getCurrentUser().getUid();
                mCurrent_phone = mAuth.getCurrentUser().getPhoneNumber();

                final DatabaseReference chatRef = mFirebaseDatabase.getReference(Constants.CHAT_LOCATION);
                final DatabaseReference messageRef = mFirebaseDatabase.getReference(Constants.MESSAGE_LOCATION);
                DatabaseReference pushRef = chatRef.push();
                pushKey = pushRef.getKey();
                mCurrentUserDatabaseReference = FirebaseDatabase.getInstance().getReference().child(Constants.USERS_LOCATION).child(mCurrent_user_id);
                mChat.setUid(pushKey);
                mChat.setTitlu_intrebare(input_field.getText().toString());
                Log.e(TAG, "Push key is: " + pushKey);


                //Create HashMap for Pushing Conv
                HashMap<String, Object> chatItemMap = new HashMap<String, Object>();
                HashMap<String,Object> chatObj = (HashMap<String, Object>) new ObjectMapper()
                        .convertValue(mChat, Map.class);
                chatItemMap.put("/" + pushKey, chatObj);
                chatRef.updateChildren(chatItemMap);

                //Create corresponding message location for this chat
                Chat initialMessages =
                        new Chat(mTitle,  mCurrent_phone, mCurrent_user_id, null, new  Date().getTime());
                final DatabaseReference initMsgRef =
                        mFirebaseDatabase.getReference(Constants.MESSAGE_LOCATION + "/" + pushKey);
                final DatabaseReference msgPush = initMsgRef.push();
                final String msgPushKey = msgPush.getKey();
                initMsgRef.child(msgPushKey).setValue(initialMessages);


                //Must add chat reference under every user object. Chat/User/Chats[chat1, chat2 ..]
                //Add to current users chat object
                // but this would require more complex queries on other pages
                chatItemMap = new HashMap<String, Object>();
                chatItemMap.put("/intrebari/" + pushKey, chatObj); //repushes chat obj -- Not space efficient
                mCurrentUserDatabaseReference.updateChildren(chatItemMap); //Adds Chatkey to users chats

                Intent intent = new Intent(QuestionListActivity.this, ChatActivity.class);
                String messageKey = pushKey;
                intent.putExtra(Constants.MESSAGE_ID, messageKey);
                intent.putExtra(Constants.CHAT_NAME, mChat.getTitlu_intrebare());
                startActivity(intent);
            }
        });

        builder.setNegativeButton("Anuleaza", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        builder.show();
    }
}
