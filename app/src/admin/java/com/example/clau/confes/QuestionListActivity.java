package com.example.clau.confes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.clau.confes.Adapters.ConvList;
import com.example.clau.confes.Objects.Conv;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import Utils.Constants;

public class QuestionListActivity extends AppCompatActivity {

    //Screen Views
    private ListView  mChatListView;
    private static final String TAG = "MainActivity";

    //Variables
    private String mCurrent_user_id;
    private String mCurrent_phone;
    private String mTitle;
    private String pushKey;

    private List<Conv> convList;
    private ArrayList<String> list_of_rooms = new ArrayList<>();
    private Conv mChat;
    private ValueEventListener mValueEventListener;

    //Firebase
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mChatDatabaseReference;
    private DatabaseReference mCurrentUserDatabaseReference;
    private DatabaseReference mUserDatabaseReference;
    private DatabaseReference mConvDatabase;
    private FirebaseListAdapter mChatAdapter;
    private FirebaseRecyclerAdapter mAdapter;
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Lista Intrebari");

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mChat = new Conv("","", false);

        //Initialize screen variables
        mChatListView = (ListView) findViewById(R.id.chatListView);
        convList = new ArrayList<>();
        mChatDatabaseReference = mFirebaseDatabase.getReference().child(Constants.CHAT_LOCATION);
        //Add on click listener to line items
        mChatListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //getting the selected artist
                Conv conv = convList.get(position);

                Intent intent = new Intent(QuestionListActivity.this, ChatActivity.class);
                intent.putExtra(Constants.MESSAGE_ID, conv.getUid());
                intent.putExtra(Constants.CHAT_NAME, conv.getTitlu_intrebare());
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        mChatDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                convList.clear();
                for (DataSnapshot convSnapshot : dataSnapshot.getChildren()){
                    Conv conv = convSnapshot.getValue(Conv.class);
                    convList.add(conv);
                }
                ConvList adapter = new ConvList(QuestionListActivity.this, convList);
                mChatListView.setAdapter(adapter);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void onBackPressed(){
        // Your Code Here. Leave empty if you want nothing to happen on back press.
    }
}

