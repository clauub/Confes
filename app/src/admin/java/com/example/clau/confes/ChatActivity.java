package com.example.clau.confes;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.clau.confes.Adapters.MessageAdapter;
import com.example.clau.confes.Objects.Chat;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import Utils.Constants;


public class ChatActivity extends AppCompatActivity {

    private String mChat;
    private String messageId;
    private String mCurrentUserPhone;
    private String mUserID;

    private ImageButton mChatSendBtn;
    private EditText mChatMessageView;

    private ListView mMessageListView;
    private MessageAdapter mMessageAdapter;
    private LinearLayoutManager mLinearLayout;
    private final List<Chat> messagesList = new ArrayList<>();
    private MessageAdapter mAdapter;

    // Firebase
    private FirebaseAuth mAuth;
    //Firebase Instance Variable
    private FirebaseDatabase mFirebaseDatabase ;
    private DatabaseReference mMessagesDatabaseReference ;
    private ChildEventListener mChildEventListener ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        messageId = getIntent().getStringExtra(Constants.MESSAGE_ID);
        mChat = getIntent().getStringExtra(Constants.CHAT_NAME);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(mChat);

        mMessageListView = (ListView) findViewById(R.id.messageListView);
        mChatSendBtn = (ImageButton) findViewById(R.id.chat_send_btn);
        mChatMessageView = (EditText) findViewById(R.id.chat_message_view);

        // Initialize message ListView and its adapter
        final List<Chat> friendlyMessages = new ArrayList<>();
        mMessageAdapter = new MessageAdapter(this, R.layout.item_message, friendlyMessages);
        mMessageListView.setAdapter(mMessageAdapter);
        mLinearLayout = new LinearLayoutManager(this);


        //Intializing Firebase Component
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mMessagesDatabaseReference = mFirebaseDatabase.getReference().child("messages").child(messageId);
        mAuth = FirebaseAuth.getInstance();
        mCurrentUserPhone = mAuth.getCurrentUser().getPhoneNumber();
        mUserID = mAuth.getCurrentUser().getUid();


        mChatSendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
                // Clear input box
                mChatMessageView.setText("");
            }
        });

        mChatMessageView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                if(mChatMessageView.length() == 0) {
                    mChatSendBtn.setColorFilter(ChatActivity.this.getResources().getColor(R.color.common_google_signin_btn_text_dark_disabled));
                    mChatSendBtn.setEnabled(false); //disable send button if no text entered
                }else {
                    mChatSendBtn.setColorFilter(ChatActivity.this.getResources().getColor(R.color.colorPrimary));
                    mChatSendBtn.setEnabled(true);//otherwise enable
                }
            }
        });
        if(mChatMessageView.length() == 0)mChatSendBtn.setEnabled(false);

        mChildEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Chat message = dataSnapshot.getValue(Chat.class);
//        Getting Message From DataBase and Display in List View
                mMessageAdapter.add(message);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        mMessagesDatabaseReference.addChildEventListener(mChildEventListener);
    }

    private void sendMessage() {

        String message = mChatMessageView.getText().toString();

        Chat messages = new Chat(message, mCurrentUserPhone, mUserID, mChat, new  Date().getTime());
        mMessagesDatabaseReference.push().setValue(messages);
    }


}

