package com.example.clau.confes.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.clau.confes.Objects.Chat;
import com.example.clau.confes.Objects.Conv;
import com.example.clau.confes.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.List;

import Utils.Constants;

/**
 * Created by Clau on 2/9/2018.
 */

public class ConvList extends ArrayAdapter<Conv> {
    private Activity context;
    List<Conv> convList;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseDatabase mFirebaseDatabase;
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("HH:mm");


    public ConvList(Activity  context,  List<Conv> convList) {
        super(context, R.layout.conv_layout, convList);
        this.context = context;
        this.convList = convList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.conv_layout, null, true);

        TextView titleTextView = (TextView) listViewItem.findViewById(R.id.titleTextView);
        final TextView msgTextView = (TextView) listViewItem.findViewById(R.id.msgTextView);
        final TextView timeTextView = (TextView) listViewItem.findViewById(R.id.timeTextView);

        Conv conv = convList.get(position);

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference messageRef = mFirebaseDatabase.getReference(Constants.MESSAGE_LOCATION + "/" + conv.getUid());

        messageRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Chat newMsg = dataSnapshot.getValue(Chat.class);
                msgTextView.setText(newMsg.getText());
                timeTextView.setText(SIMPLE_DATE_FORMAT.format(newMsg.getTime()));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        titleTextView.setText(conv.getTitlu_intrebare());
        return listViewItem;
    }
}
