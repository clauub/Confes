package com.example.clau.confes.Objects;

import com.google.firebase.database.Exclude;

/**
 * Created by Clau on 1/24/2018.
 */

public class Admin {

    @Exclude
    public String phone;
    public String userID;
    public Boolean isUser;

    public Admin() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public Admin(String phone, String userID, Boolean isUser) {
        this.phone = phone;
        this.userID = userID;
        this.isUser = isUser;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public Boolean getIsUser() {
        return isUser;
    }

    public void setIsUser(Boolean isUser) {
        isUser = isUser;
    }
}
