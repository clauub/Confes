package Utils;

/**
 * Created by Clau on 1/26/2018.
 */

public final class Constants {

    //Location of Firebase resources
    public static final String USERS_LOCATION = "users";
    public static final String CHAT_LOCATION = "intrebari";
    public static final String MESSAGE_LOCATION = "messages";

    public static final String MESSAGE_ID = "MID";
    public static final String CHAT_NAME = "CNAME";
}
